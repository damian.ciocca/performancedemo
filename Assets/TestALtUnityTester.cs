﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestALtUnityTester : MonoBehaviour
{
    public Text yesNoAlt;
    public Text yesNoAuto;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("IsDebug: " + Debug.isDebugBuild);
        
#if ENV_AUTOMATION
        yesNoAuto.text = "YES";
#else
        yesNoAuto.text = "NO";
#endif

#if ALTUNITYTESTER
        yesNoAlt.text = "YES";
#else
        yesNoAlt.text = "NO";
#endif
    }
}
﻿
using System.Linq;
#if UNITY_IOS
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
 
public class PostProcess : IPostprocessBuild {
    public int callbackOrder { get; private set; }
    public void OnPostprocessBuild(BuildTarget buildTarget, string pathToBuiltProject)
    {
        // paths
        var projectFileName = Path.GetFileNameWithoutExtension(Directory.GetDirectories(pathToBuiltProject)
            .First(d => Path.HasExtension(d) && Path.GetExtension(d) == ".xcodeproj"));
        var xCodeProjFolderPath = $"{pathToBuiltProject}/{projectFileName}.xcodeproj";
        var xcSettingsPath = $"{xCodeProjFolderPath}/project.xcworkspace/xcshareddata/WorkspaceSettings.xcsettings";
        Debug.Log($"xCodeProjFolderPath: {xCodeProjFolderPath}");
        Debug.Log($"xcSettingsPath: {xcSettingsPath}");
        // change the xcode project to use the new build system, without doing this can not compile and get an error in xcode, plus the legacy build system is now deprecated
        var xcSettingsDoc = new PlistDocument();
        xcSettingsDoc.ReadFromString( File.ReadAllText( xcSettingsPath ) );
        var xcSettingsDict = xcSettingsDoc.root;
        var xcSettingsValues = xcSettingsDict.values;
        var buildSystemTypeKey = "BuildSystemType";
        if ( xcSettingsValues.ContainsKey( buildSystemTypeKey ) ) {
            xcSettingsValues.Remove( buildSystemTypeKey ); // the removal of this key/value pair <key>BuildSystemType</key><string>Original</string> allows xcode to use the default new build system setting
        }
        File.WriteAllText( xcSettingsPath, xcSettingsDoc.WriteToString() );
    }
}
#endif
